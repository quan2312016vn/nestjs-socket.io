export type SocketResponse = {
    event: string,
    broadcast: boolean,
    data: string
} | string;