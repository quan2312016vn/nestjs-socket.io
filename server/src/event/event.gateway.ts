import { MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { SocketResponse } from './types';

@WebSocketGateway(3001)
export class EventGateway {
  @WebSocketServer()
  private server: Server;

  @SubscribeMessage('chat')
  handleMessage(@MessageBody() data: string) {
    const response: SocketResponse = {
      event: "chat",
      broadcast: true,
      data: data
    }
    
    return response;
  }

  handleConnection(client: any) {
    console.log("connected");
  }
}
