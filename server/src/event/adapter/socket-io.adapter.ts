import { Server, Socket } from "socket.io";
import { MessageMappingProperties } from "@nestjs/websockets";
import { WebSocketAdapter, INestApplicationContext } from "@nestjs/common";
import { Observable, fromEvent } from "rxjs";
import { mergeMap, filter } from "rxjs/operators";
import { SocketResponse } from "./../types";

export class SocketIOAdapter implements WebSocketAdapter {
    constructor(private app: INestApplicationContext) {

    }

    create(port: number, options: any | {namespace: string} = {}) {
        const { namespace } = options;

        const serverOptions = {
            cors: {
                origin: "*",
                methods: ["GET", "POST"]
            }
        }

        const io = namespace ? new Server(port, serverOptions).of(namespace)
                            : new Server(port, serverOptions);

        return io;
    }

    bindClientConnect(server: Server, callback: any) {
        server.on("connection", callback);
    }

    bindMessageHandlers(
        client: Socket,
        handlers: MessageMappingProperties[], 
        process: (data: any) => Observable<any>
    ) {
        handlers.forEach(({ message, callback }) => {
            fromEvent(client, message)
            .pipe(
                mergeMap(data => process(callback(data))),
                filter(result => result)
            )
            .subscribe((response: SocketResponse) => {
                if (typeof response === "string") {
                    return client.emit(message, response);
                }
               
                const { event, data, broadcast } = response;

                if (broadcast) {
                    return client.broadcast.emit(event, data);
                }
                
                return client.emit(event, data);
            })
        });
    } 

    close(server: Server) {
        server.close();
    }
}