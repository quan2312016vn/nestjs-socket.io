import { html, css, LitElement } from "lit";
import { property } from "lit/decorators.js";
import "./components/ChatBoxComponent";
import socket from "./config/socket-io.config";

export class App extends LitElement {
    static styles = css`
    
    `;

    @property({ reflect: true })
    name: string = "World";

    render() {
        return html`
            <h1>Hello ${this.name}</h1>
            <chat-box></chat-box>
        `;
    }

    chatClick() {
        socket.emit("chat", "hello");
    }
}
