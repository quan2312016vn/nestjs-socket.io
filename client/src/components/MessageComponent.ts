import { html, css, LitElement } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement("message-element")
class MessageComponent extends LitElement {
    static styles = css`
    
    `;

    @property({ reflect: true })
    content: string = "";

    render() {
        return html`
            <p>${this.content}</p>
        `
    }
}
