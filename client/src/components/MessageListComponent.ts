import { html, css, LitElement } from "lit";
import { customElement, property } from "lit/decorators.js";
import "./MessageComponent";
import socket from "../config/socket-io.config";

@customElement("message-list")
class MessageListComponent extends LitElement {
    static styles = css`
        :host {
            display: block;
            border: 1px solid black;
            width: 250px;
            height: 300px;
            overflow-y: scroll;
            overflow-wrap: break-word;
        }
    `;

    @property()
    listMessage: string[] = [];

    render() {
        return html`
            ${
                this.listMessage.map(message => {
                    return html`
                        <message-element content="${message}"></message-element>
                    `
                })
            }
        `
    }

    async updated() {
        await this.updateComplete
        this.scrollTop = this.scrollHeight * 2;
    }
}
