import { html, css, LitElement } from "lit";
import { customElement, property, query } from "lit/decorators.js";

@customElement("message-form")
class MessageFormComponent extends LitElement {
    static styles = css`
    
    `;

    @query("#input")
    declare input: HTMLInputElement;

    @property()
    declare sendMessage: Function;

    render() {
        return html`
            <input type="text" id="input">
            <button @click=${this.handleSendClick}>Send</button>
        `
    }

    handleSendClick() {
        this.sendMessage(this.input);
    }
}
