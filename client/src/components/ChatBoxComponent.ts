import { html, css, LitElement } from "lit";
import { customElement, property } from "lit/decorators.js";
import socket from "../config/socket-io.config";
import "./MessageFormComponent"
import "./MessageListComponent"

@customElement("chat-box")
class ChatBoxComponent extends LitElement {
    constructor() {
        super();

        socket.on("chat", (data: string) => {
            this.listMessage = [...this.listMessage, "Other: " + data];
        });
    }
    
    static styles = css`
        
    `;

    @property({ 
        state: true ,
        hasChanged: (oldProp, newProp) => {
            return JSON.stringify(oldProp) !== JSON.stringify(newProp);
        }
    })
    listMessage: string[] = [];

    render() {
        return html`
            <message-list .listMessage=${this.listMessage}></message-list>
            <message-form .sendMessage=${this.sendMessage.bind(this)}></message-form>
        `
    }

    sendMessage(input: HTMLInputElement) {
        const data = input.value;
        socket.emit("chat", data);

        this.listMessage = [...this.listMessage, "You: " + data];
        input.value = "";
    }
}
